# Product: Vijelu version 0.1.0 by v-v.icu

Free for personal and commercial use under the CCA 4.0 license (http://creativecommons.org/licenses/by/4.0/)

## Credits:

Demo Images:
	Pexels (pexels.com/)
- https://www.pexels.com/photo/photo-of-woman-in-black-sunglasses-posing-by-blue-wall-2294430/
- https://www.pexels.com/photo/photo-of-woman-wearing-eyeglasses-3822080/
- https://www.pexels.com/photo/photo-of-a-man-3139606/
- https://www.pexels.com/photo/adult-beautiful-casual-cute-372042/
- https://www.pexels.com/photo/man-in-blue-sweater-having-a-sweet-conversation-3783229/
- https://www.pexels.com/photo/person-playing-chess-1040157/
- https://www.pexels.com/photo/group-of-people-sitting-inside-room-2422294/
- https://www.pexels.com/photo/bridge-with-train-under-blue-sky-775482/
- https://www.pexels.com/photo/blue-and-green-elephant-with-light-1680755/

Icons:
- Font Awesome (fontawesome.io)

Other:
- jQuery (jquery.com)
- Bootstrap (getbootstrap.com)

v-v.icu  :: hello@v-v.icu
LinkedIn :: https://www.linkedin.com/company/v-v-icu
Twitter	 :: @v_v_icu :: https://twitter.com/v-v_icu
Facebook :: https://www.facebook.com/vv.icu.face